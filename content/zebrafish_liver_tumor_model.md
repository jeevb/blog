Title: Zebrafish as a Model Organism For Cancer Research
Date: 2015-05-22 10:00
Category: research
Tags: cancer, imaging

A colleague of mine recently shared the following stunning image of a transgenic zebrafish with B-catenin-induced liver tumor. This image was tagged to a [recent submission](http://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1005305) to [PLOS Genetics](http://journals.plos.org/plosgenetics/).

![zebrafish](http://i.imgur.com/DPLl3uS.png 'Zebrafish with liver tumor')
