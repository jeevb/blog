Title: Differential Expression Analysis with the limma R package
Date: 2015-08-07 12:00
Category: bioinformatics
Tags: sequencing, limma, R

### Introduction

Let's assume that we are starting with a SummarizedExperiment object containing an (n x m) matrix of raw counts overlapping a set of annotations/features, where n (rows) corresponds to the number of features, and m (columns) corresponds to the number of samples. **Note: This will work for ExpressionSet objects as well.**

Let's explore our SummarizedExperiment object, `overlaps`:
```r
> overlaps
class: SummarizedExperiment
dim: 6186 15
exptData(0):
assays(1): counts
rownames(6186): ENSMUSG00000064842 ENSMUSG00000088333 ...
  ENSMUSG00000064371 ENSMUSG00000064372
rowRanges metadata column names(0):
colnames(15): A-2-1_L1.LB5 A-2-2_L1.LB6 ... CTL2_L1.LB3 CTL5_L1.LB4
colData names(1): group
```
```r
> colData(overlaps)
DataFrame with 15 rows and 1 column
                      group
                   <factor>
A-2-1_L1.LB5          cond1
A-2-2_L1.LB6          cond1
A-2-41_reg_L1.LB10    cond2
A-2-43_reg_L1.LB8     cond2
A-2-44_reg_L1.LB9     cond2
...                     ...
B-1-7_reg_L1.LB14     cond2
B-1-9_L1.LB11         cond1
CTL1_L1.LB2         control
CTL2_L1.LB3         control
CTL5_L1.LB4         control
```

### Using *limma* with a SummarizedExperiment object

First you will have to install and load the [*cRumbs*](https://bitbucket.org/jeevb/crumbs) package containing some of my most commonly used snippets. Installation instructions are available on the README file at the above link.
```r
> library(cRumbs)
```

From here, it is as simple as issuing a single one-liner to perform differential gene expression:
```r
> res <- DiffEqFromRaw(overlaps, design = ~0 + group, groupcond1 - groupcontrol)
```

Let's explore the above call:

- `DiffEqFromRaw`: this is the correct function to use when dealing with raw counts. If you have log2-transformed expression values, use `DiffEq` instead.
- `overlaps`: this is our SummarizedExperiment object containing our raw counts.
- `design = ~0 + group`: this is our design formula specifying `group` as a covariate. If you wish to adjust for additional factors just throw them at the back as follows: `~0 + group + batch`.
- `groupcond1 - groupcontrol`: this is the format for specifying the contrast to determine the differences between `cond1` and `control` with respect to the `group` attribute/covariate.

The result of the above function call is a list of dataframes, with each element of the list corresponding to an individual contrast or pairwise comparison:
```r
> str(res)
List of 1
 $ groupcond1 - groupcontrol:'data.frame':  1192 obs. of  5 variables:
  ..$ gene : chr [1:1192] "ENSMUSG000000XXXXX" "ENSMUSG000000XXXXX" "ENSMUSG000000XXXXX" "ENSMUSG000000XXXXX" ...
  ..$ logFC: num [1:1192] -8.18 5.06 4.65 -6.04 -4.56 ...
  ..$ t    : num [1:1192] -15.9 12.9 12.6 -12.2 -11.8 ...
  ..$ pval : num [1:1192] 2.58e-12 9.34e-11 1.36e-10 2.32e-10 4.00e-10 ...
  ..$ fdr  : num [1:1192] 3.07e-09 5.39e-08 5.39e-08 6.92e-08 9.53e-08 ...
```

As you can see, each dataframe consists of 5 columns: `gene`, `logFC`, `t`, `pval`, `fdr`. `t` refers to the t-statistic for each gene-wise test. This may come in handy when doing pathway enrichment analysis.

Here is a better view of the dataframe:
```r
> head(res[['groupcond1 - groupcontrol']])
                gene     logFC         t         pval          fdr
1 ENSMUSG000000XXXXX -8.182929 -15.86752 2.578272e-12 3.073301e-09
2 ENSMUSG000000XXXXX  5.061198  12.88346 9.338601e-11 5.388304e-08
3 ENSMUSG000000XXXXX  4.645152  12.60203 1.356117e-10 5.388304e-08
4 ENSMUSG000000XXXXX -6.044095 -12.20464 2.323060e-10 6.922719e-08
5 ENSMUSG000000XXXXX -4.561068 -11.81434 3.995458e-10 9.525172e-08
6 ENSMUSG000000XXXXX -4.730157 -11.59967 5.416145e-10 1.076007e-07
```

### Specifying multiple contrasts

Recall that we had to use `res[['groupcond1 - groupcontrol']]` to retrieve the first element of the list - containing the statistics for our comparison of interest.

On that note, I should mention that one may also perform queries for multiple contrasts by simplying stringing them to the back of the function call, as follows:
```r
> res <- DiffEqFromRaw(overlaps, design = ~0 + group, groupcond1 - groupcontrol, groupcond2 - groupcontrol, groupcond2 - groupcond1)
```

Here is what that looks like:
```r
> lapply(res, head)
$`groupcond1 - groupcontrol`
                gene     logFC         t         pval          fdr
1 ENSMUSG000000XXXXX -8.182929 -15.86752 2.578272e-12 3.073301e-09
2 ENSMUSG000000XXXXX  5.061198  12.88346 9.338601e-11 5.388304e-08
3 ENSMUSG000000XXXXX  4.645152  12.60203 1.356117e-10 5.388304e-08
4 ENSMUSG000000XXXXX -6.044095 -12.20464 2.323060e-10 6.922719e-08
5 ENSMUSG000000XXXXX -4.561068 -11.81434 3.995458e-10 9.525172e-08
6 ENSMUSG000000XXXXX -4.730157 -11.59967 5.416145e-10 1.076007e-07

$`groupcond2 - groupcontrol`
                gene     logFC         t         pval          fdr
1 ENSMUSG000000XXXXX -7.406817 -14.36256 1.457022e-11 1.736771e-08
2 ENSMUSG000000XXXXX  5.412735  11.85493 3.773960e-10 1.549902e-07
3 ENSMUSG000000XXXXX  6.947615  11.83140 3.900761e-10 1.549902e-07
4 ENSMUSG000000XXXXX -5.542669 -11.19033 9.792781e-10 2.678853e-07
5 ENSMUSG000000XXXXX  4.358501  11.09686 1.123680e-09 2.678853e-07
6 ENSMUSG000000XXXXX -3.962091 -10.25913 4.011775e-09 7.970059e-07

$`groupcond2 - groupcond1`
                gene     logFC         t         pval          fdr
1 ENSMUSG000000XXXXX -2.713438 -9.541068 1.268771e-08 1.512375e-05
2 ENSMUSG000000XXXXX -2.437226 -7.979908 1.918880e-07 1.143652e-04
3 ENSMUSG000000XXXXX -3.493571 -7.572864 4.101770e-07 1.629770e-04
4 ENSMUSG000000XXXXX -3.384537 -7.269848 7.325508e-07 2.183001e-04
5 ENSMUSG000000XXXXX -2.714470 -6.748889 2.043920e-06 4.569170e-04
6 ENSMUSG000000XXXXX -3.475916 -6.547890 3.066557e-06 4.569170e-04
```

### Constructing a Heatmap

One may also construct heatmaps from the voom-transformed expression values. These are returned as the `voom` attribute of the `res` object. They may be accessed as follows:
```r
> transformed <- attr(res, 'voom')
```

Since the phenotype attribute data is lost from `transformed`, you may pass sample annotations (to color the columns of the heatmap) directly from the original SummarizedExperiment object, `overlaps`.

Again, using the `HeatWrap` wrapper from the *cRumbs* package:
```r
> pdf('heatmap.pdf')
> cRumbs::HeatWrap(transformed, by = colData(overlaps)$group, margins = c(10, 10))
> dev.off()
```

This generates the following heatmap:  
![heatmap](http://i.imgur.com/1ge3nYa.png 'heatmap.pdf')
