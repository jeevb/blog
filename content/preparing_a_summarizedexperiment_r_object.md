Title: Preparing a SummarizedExperiment R Object
Date: 2015-08-07 11:00
Category: bioinformatics
Tags: R


### Introduction

Considering how ubiquitously I use SummarizedExperiment (or ExpressionSet) objects in my pipelines, I figured it'd be worth writing up how to create usable objects from raw `.csv` files.

### Reading in Expression Data

Let's assume that we have a comma-separated file containing raw, normalized, or transformed expression values, `exprs.csv`:

![exprs.csv](http://i.imgur.com/t6N9rGy.png)

Let's read this into R:
```r
> exprs <- read.csv('exprs.csv', row.names = 1, check.names = FALSE)
```

This should yield an (n x m) matrix where n (rows) corresponds to the number of genes, and m (columns) corresponds to the number of samples.

### Reading in Phenotype Attributes

We will also need a comma-separated file containing phenotype attributes or annotations for the respective samples, `pheno.csv`:

![pheno.csv](http://i.imgur.com/cAGUs7y.png)

Let's read this into `pheno`:
```r
> pheno <- read.csv('pheno.csv', as.is = TRUE, row.names = 1)
```

This should yield a matrix with m rows, with each row corresponding to an individual sample, and X attributes.

### Preparing the SummarizedExperiment Object

Now that we have our required elements: `exprs` and `pheno`, we are ready to start setting up the SummarizedExperiment object.

**FIRST**, we need a sanity check: `colnames(exprs)` should be exactly the same as `rownames(pheno)`. Here's how to check this quickly:
```r
> all(colnames(exprs) == rownames(pheno))
```

If this is `TRUE`, we are all set and ready to proceed. If this isn't the case:

- You have a sample mismatch, and will need to check your original files.
- The order of the samples in `exprs` and `pheno` differ. This may be rectified by doing the following: `pheno <- pheno[match(colnames(exprs), rownames(pheno)), , drop = FALSE]`

Let's make sure all is well by performing the sanity check again:
```r
> all(colnames(exprs) == rownames(pheno))
[1] TRUE
```

Great!

Now all we need to do is load up the [*GenomicRanges*](http://bioconductor.org/packages/release/bioc/html/GenomicRanges.html) R package and compile the SummarizedExperiment object, `dat`:
```r
> library(GenomicRanges)
> dat <- SummarizedExperiment(assay = data.matrix(exprs), colData = DataFrame(pheno))
```

Let's take a second to pat ourselves on the back for this:
```r
> dat
class: SummarizedExperiment
dim: 6186 15
exptData(0):
assays(1): ''
rownames(6186): ENSMUSG00000064842 ENSMUSG00000088333 ...
  ENSMUSG00000064371 ENSMUSG00000064372
rowRanges metadata column names(0):
colnames(15): A-2-1_L1.LB5 A-2-2_L1.LB6 ... CTL2_L1.LB3 CTL5_L1.LB4
colData names(1): group
```
