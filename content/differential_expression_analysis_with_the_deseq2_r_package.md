Title: Differential Expression Analysis with the DESeq2 R package
Date: 2015-07-31 18:00
Category: bioinformatics
Tags: sequencing, deseq2, R

### Introduction

In the interest of establishing a reasonable starting point for this blog post, let's assume we have 2 files: `counts.rda` and `phenotype_data.csv` as follows:
```
$ ls
counts.rda  phenotype_data.csv
```

`counts.rda` would be the endpoint of the [*staR*](https://bitbucket.org/jeevb/star) package - A SummarizedExperiment R object containing an (n x m) matrix of raw counts overlapping a set of annotations/features, where n (rows) corresponds to the number of features, and m (columns) corresponds to the number of samples.

`phenotype_data.csv` would specify a set of attributes associated with each sample. This could, for instance, contain treatment information, presence or absence of a mutation, control or overexpression groups, etc. For the purpose of this blog post, I have defined one attribute - group - that specifies whether a given sample belongs to a `control`, `cond1` or `cond2` group, as follows:
```
$ head phenotype_data.csv
"","group"
"A-2-1_L1.LB5","cond1"
"A-2-2_L1.LB6","cond1"
"A-2-41_reg_L1.LB10","cond2"
"A-2-43_reg_L1.LB8","cond2"
"A-2-44_reg_L1.LB9","cond2"
"A-2-4_L1.LB7","cond1"
"B-1-10_reg_L1.LB15","cond2"
"B-1-4_L1.LB12","cond1"
"B-1-5_L1.LB13","cond1"
```

### The R Environment

We will be using R for all downstream analyses as part of this blog post, so now would be a great time to fire it up:
```r
$ R

R version 3.2.1 (2015-06-18) -- "World-Famous Astronaut"
Copyright (C) 2015 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

>
```

I am launching R directly from the console, but you may do so using the R gui or [Rstudio](https://www.rstudio.com/). I am also running the latest version of R - 3.2.1, and you should be as well unless you have an absolutely compelling reason not to.

### Bioconductor and the *DESeq2* R Package

We will be using the *DESeq2* R package for differential expression analysis in this blog post, but there are other packages that you may use as well, including but not limited to *edgeR* and *limma*.

Let's get *DESeq2* installed:
```r
> source('http://bioconductor.org/biocLite.R')
Bioconductor version 3.1 (BiocInstaller 1.18.4), ?biocLite for help
> biocLite('DESeq2')
```

Assuming the installation completes without any errors, you should now have *DESeq2* installed. You may load it into R as such:
```r
> library(DESeq2)
```

### Preparing the SummarizedExperiment object

You will first need to load the `counts.rda` file into R:
```r
> load('counts.rda')
```
```r
> overlaps
class: SummarizedExperiment
dim: 6186 15
exptData(0):
assays(1): counts
rownames(6186): ENSMUSG00000064842 ENSMUSG00000088333 ...
  ENSMUSG00000064371 ENSMUSG00000064372
rowRanges metadata column names(0):
colnames(15): A-2-1_L1.LB5 A-2-2_L1.LB6 ... CTL2_L1.LB3 CTL5_L1.LB4
colData names(0):
```

We have determined the raw counts for 6186 features and 15 samples, according to the summary of the `overlaps` object that was contained within `counts.rda`. However, `overlaps` **DOES NOT** contain the phenotype attributes - the `colData` slot is empty:
```r
> colData(overlaps)
DataFrame with 15 rows and 0 columns
```

However, we already do have the phenotype attributes. We will just have to feed them into `overlaps`. Let's read in the `phenotype_data.csv` file that we have:
```r
> pheno <- read.csv('phenotype_data.csv', as.is = TRUE, row.names = 1)
```
```r
> head(pheno)
                   group
A-2-1_L1.LB5       cond1
A-2-2_L1.LB6       cond1
A-2-41_reg_L1.LB10 cond2
A-2-43_reg_L1.LB8  cond2
A-2-44_reg_L1.LB9  cond2
A-2-4_L1.LB7       cond1
```

As we discussed previously, we have 3 different groups of samples:
```r
> table(pheno$group)

  cond1   cond2 control
      6       6       3
```

We will need to set the appropriate levels for these groups. Let's make `control` the baseline group, followed by `cond1` and `cond2` respectively. You may do this as follows:
```r
> pheno$group <- factor(pheno$group, levels = c('control', 'cond1', 'cond2'))
```

Now that the `pheno` dataframe containing the phenotype attributes for the respective samples is ready, we can add it to the `colData` slot of `overlaps`:
```r
> colData(overlaps) <- DataFrame(pheno)
```
```r
> overlaps
class: SummarizedExperiment
dim: 6186 15
exptData(0):
assays(1): counts
rownames(6186): ENSMUSG00000064842 ENSMUSG00000088333 ...
  ENSMUSG00000064371 ENSMUSG00000064372
rowRanges metadata column names(0):
colnames(15): A-2-1_L1.LB5 A-2-2_L1.LB6 ... CTL2_L1.LB3 CTL5_L1.LB4
colData names(1): group
```
```r
> colData(overlaps)
DataFrame with 15 rows and 1 column
                         group
                   <character>
A-2-1_L1.LB5             cond1
A-2-2_L1.LB6             cond1
A-2-41_reg_L1.LB10       cond2
A-2-43_reg_L1.LB8        cond2
A-2-44_reg_L1.LB9        cond2
...                        ...
B-1-7_reg_L1.LB14        cond2
B-1-9_L1.LB11            cond1
CTL1_L1.LB2            control
CTL2_L1.LB3            control
CTL5_L1.LB4            control
```

We have successfully loaded the phenotype attributes into the `overlaps` object, and are now ready to perform differential expression analysis

### Using *DESeq2* with a SummarizedExperiment object

The first step in running a differential expression analysis with *DESeq2* is to set up an object of class `DESeqDataSet`:
```r
> dds <- DESeqDataSet(overlaps, design = ~ group)
```
```r
> class(dds)
[1] "DESeqDataSet"
attr(,"package")
[1] "DESeq2"
```

You will have to specify a formula for the design argument. In this simple example, we only had one attribute that we wanted to use to perform differential expression analysis, and we have included that in the formula: `~ group`.

One may also use more complex models. For instance, if you wanted to adjust for differences between different batches of samples while calculating the effect of a particular treatment, you would use `~ batch + treatment` where `batch` and `treatment` are attributes that should already be specified in the `colData` dataframe. You always want to add your attribute of interest to the end of the formula - in this example, we are interested in the effect of the treatment.

Once you have set up the `DEseqDataSet` object, the next step is to fit a model. You may do that as follows:
```r
> dds <- DESeq(dds)
```

At this point, all that remains to be done is to perform a pairwise comparison of interest. For the purpose of this blog post, we will calculate the difference between the `cond1` and `control` groups:
```r
res <- results(dds, c('group', 'cond1', 'control'))
```

Here, we are using `control` as the denominator/baseline and calculating the difference between `cond1` and the baseline, as defined by the `group` phenotype attribute. We can preview the top most different genes after sorting the results by `pvalue`:
```r
> res <- res[order(res$pvalue), ]
```
```r
> head(res)
log2 fold change (MAP): group cond1 vs control
Wald test p-value: group cond1 vs control
DataFrame with 6 rows and 6 columns
                     baseMean log2FoldChange     lfcSE      stat       pvalue
                    <numeric>      <numeric> <numeric> <numeric>    <numeric>
ENSMUSG000000XXXXX   1462.005       4.366581 0.3179416  13.73391 6.360727e-43
ENSMUSG000000XXXXX 699720.638      -6.358291 0.4750395 -13.38476 7.423396e-41
ENSMUSG000000XXXXX   8210.152      -4.745426 0.3630663 -13.07041 4.860206e-39
ENSMUSG000000XXXXX   7691.647      -4.886634 0.3919661 -12.46698 1.130254e-35
ENSMUSG000000XXXXX   4161.109       4.738235 0.3826826  12.38163 3.285910e-35
ENSMUSG000000XXXXX  31389.369      -7.777589 0.6419434 -12.11569 8.722359e-34
                           padj
                      <numeric>
ENSMUSG000000XXXXX 1.000542e-39
ENSMUSG000000XXXXX 5.838501e-38
ENSMUSG000000XXXXX 2.548368e-36
ENSMUSG000000XXXXX 4.444722e-33
ENSMUSG000000XXXXX 1.033747e-32
ENSMUSG000000XXXXX 2.286712e-31
```

As interesting as these results may be, they are difficult to interpret without looking up each of the ensembl gene IDs. We can circumvent this issue by adding human-friendly gene names to the table. However, we will first have to extract the gene names corresponding to the ensembl gene IDs. You may do so with the help of the [*staR*](https://bitbucket.org/jeevb/star) package. **Note: This is a long process, and will require a good amount (~8gb) of memory.**
```r
> library(staR)
> db <- ParseGTF('~/genomes/Mus_musculus.GRCm38.80/Mus_musculus.GRCm38.80.gtf')
```

Extract only gene annotations:
```r
> db <- Genes(db)
```

Construct a dataframe containing the ensembl gene IDs and their corresponding human-friendly symbols:
```r
> features <- data.frame(symbol = GeneNames(db), row.names = GeneIDs(db), stringsAsFactors = FALSE)
```
```r
> head(features)
                          symbol
ENSMUSG00000102693 4933401J01Rik
ENSMUSG00000064842       Gm26206
ENSMUSG00000051951          Xkr4
ENSMUSG00000102851       Gm18956
ENSMUSG00000103377       Gm37180
ENSMUSG00000104017       Gm37363
```

We can now merge this into the results from the differential expression analysis:
```r
> res <- merge(features, as.data.frame(res), by = 0, all.y = TRUE)
> res <- res[order(res$pvalue), ]
```

And **FINALLY**, we have a nice readable output:
```r
> head(res)
              Row.names    symbol   baseMean log2FoldChange     lfcSE      stat
526  ENSMUSG000000XXXXX    XXXXXX   1462.005       4.366581 0.3179416  13.73391
689  ENSMUSG000000XXXXX    XXXXXX 699720.638      -6.358291 0.4750395 -13.38476
664  ENSMUSG000000XXXXX    XXXXXX   8210.152      -4.745426 0.3630663 -13.07041
4081 ENSMUSG000000XXXXX    XXXXXX   7691.647      -4.886634 0.3919661 -12.46698
3599 ENSMUSG000000XXXXX    XXXXXX   4161.109       4.738235 0.3826826  12.38163
773  ENSMUSG000000XXXXX    XXXXXX  31389.369      -7.777589 0.6419434 -12.11569
           pvalue         padj
526  6.360727e-43 1.000542e-39
689  7.423396e-41 5.838501e-38
664  4.860206e-39 2.548368e-36
4081 1.130254e-35 4.444722e-33
3599 3.285910e-35 1.033747e-32
773  8.722359e-34 2.286712e-31
```

### Constructing a Heatmap from a DESeqDataSet object

And lastly, what is any analysis without a pretty figure to top it off, right? On that note, let's create a heatmap. There are certainly many reasons to create heatmaps, and we will have to decide on an appropriate subset of features and samples for the application of interest.

For the purpose of this blog post, we will create a simple heatmap consisting of the most differently expressed genes between `control` and `cond1`. Let's start by making a list of those genes:
```r
> genes <- res$Row.names[!is.na(res$padj) & res$padj < 0.05]
```

Next, we will have to transform our raw counts into a form that can be visualized on a heatmap. *DESeq2* offers a few options for transformation: `rlog`, `vsn` and `fpkm`. I prefer `rlog`, or the regularized log transformation, over the other methods for my heatmaps, and that is what we will use. That being said, the [*DESeq2* vignette](https://www.bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.pdf) is an amazing resource for those of you that wish to dive into the details of these methods.

To perform the regularized log transformation:
```r
> transformed <- rlog(dds)
```
```r
> transformed
class: DESeqTransform
dim: 6186 15
exptData(0):
assays(1): ''
rownames(6186): ENSMUSG00000064842 ENSMUSG00000088333 ...
  ENSMUSG00000064371 ENSMUSG00000064372
rowRanges metadata column names(6): baseMean baseVar ... dispFit
  rlogIntercept
colnames(15): A-2-1_L1.LB5 A-2-2_L1.LB6 ... CTL2_L1.LB3 CTL5_L1.LB4
colData names(2): group sizeFactor
```

Since we are most interested in the genes that are different between the `control` and `cond1` groups, we will subset accordingly:
```r
> transformed <- transformed[genes, ]
```

We are now ready to generate the heatmap. We will be using the `HeatWrap` wrapper from the [*cRumbs*](https://bitbucket.org/jeevb/crumbs) package that internally calls the `heatmap.2` function from the *gplots* R package:
```r
> pdf('control_vs_cond1_DE_genes.pdf')
> cRumbs::HeatWrap(transformed, by = 'group', margins = c(10, 10))
> dev.off()
```

This generates the following heatmap:  
![heatmap](http://i.imgur.com/fxSDXsQ.png 'control_vs_cond1_DE_genes.pdf')
