Title: Hello, World!
Date: 2015-01-01 00:00

This is a product of my following new year resolutions: to better document my thoughts, and to get more comfortable with writing.

```python
>>> print 'Hello, World!'
Hello, World!
```
