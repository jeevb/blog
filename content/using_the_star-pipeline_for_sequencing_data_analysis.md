Title: Using the staR-pipeline for Sequencing Data Analysis
Date: 2015-08-03 17:00
Category: Bioinformatics
Tags: sequencing, R, ec2

### *staR-helper*

[*staR-helper*](https://bitbucket.org/jeevb/star-helper) is a convenience wrapper to simplify some of the tasks associated with using the [*staR-pipeline*](https://bitbucket.org/jeevb/star) on Amazon's EC2 platform. It allows you to launch, show or terminate instances and to properly organize your raw reads into a staR-friendly manner, using simple one-liners.

There are two ways to download *staR-helper*:

- [Manual download](https://bitbucket.org/jeevb/star-helper/downloads)
- Using git: `git clone https://jeevb@bitbucket.org/jeevb/star-helper.git`

Once you have local copy of *staR-helper*, you will need to install the required dependencies and set up a few configuration options. First, navigate to your local `star-helper` directory:
```
$ cd ~/Workspace/Repositories/star-helper
```
You may have to change the path accordingly depending on where you downloaded/cloned the repository to. On my system, its within the `~/Workspace/Repositories` directory.

Next, you will have to make sure that you have `pip` installed. This is the package manager that you will use to install the dependencies that *staR-helper* needs:
```
$ pip --help
```
If you see a menu of sorts with some helpful information, then you already have `pip` installed and can skip the next step. On the other hand, if you see an error, do the following:
```
$ sudo easy_install pip
```

Now that `pip` is installed, you can get on with installing the dependencies:
```
$ sudo pip install -r requirements.txt
```
You have to do this from within the `star-helper` directory where the `requirements.txt` file resides.

### Configuring *staR-helper*

Now that you have successfully installed *staR-helper* and all of its dependencies, you will need to perform the initial configuration. For this you will need to have the following items handy:

- AWS access key ID
- AWS secret access key
- Keypair file
- Security group ID

If you do not already have the above-mentioned information, you should request a new account from the AWS account administrator.

While still within the `star-helper` directory, enter the following command:
```
$ ./staR-helper init
```
You will be prompted to specify the following information (that you should already have obtained from your administrator):

- AWS access key ID
- AWS secret access key
- Region - default is `us-west-2`. You may hit enter to continue with this default value.
- Image name - default is `staR-pipeline`. You may hit enter to continue with this default value.
- Keypair - the name of your keypair file without the `.pem` extension (e.g. 'mykey').
- Security groups
- Instance type - default is `c3.8xlarge`. You may hit enter to continue with this default value.

A `config.ini` file should be created in the `star-helper` directory once you are done.

### Launching an instance

Launching new `staR-pipeline` instances is as simple as issuing the following command:
```
$ ./staR-helper launch
```
You should see some output that looks like the following:
```
Launched:
ID             State          Type           IP                  Key-pair
i-78218eb1     pending        c3.8xlarge     52.26.187.136       mykey
```
**Note: You may only launch one instance at a time.**

### Connecting to an EC2 Instance

Now that you have successfully launched a *staR-pipeline* instance, you will need to gain access to it:
```
$ ssh -i ~/Downloads/mykey.pem ubuntu@52.26.187.136
```
Let's explore the above command:

- `-i ~/Downloads/mykey.pem`: this is to indicate that you will be using your assigned key file (obtained from the AWS account administrator) for authentication.
- `ubuntu`: this is the user name of the account in the newly launched instance.
- `52.26.187.136`: this is the IP of your newly launched instance. You will be provided this information when you launch the instance.

The first time you connect, you will see the following message:
```
The authenticity of host '52.26.187.136 (52.26.187.136)' can't be established.
RSA key fingerprint is a5:a8:92:04:1a:3e:1c:d8:9d:65:dd:36:a9:a1:65:ad.
Are you sure you want to continue connecting (yes/no)?
```
Type `yes` and hit enter to continue.

You should now see a prompt that looks something like this:
```
╭─ubuntu@ip-172-31-36-39  ~
╰─$
```
This indicates that you have successfully connected to the instance, and are ready to start setting up the analysis.

### Downloading the sequencing data

The first step to running the analysis is to download the raw sequencing reads. There are a few ways to accomplish this:

- `wget`
- [Cyberduck](https://cyberduck.io/)
- [Forklift](http://www.binarynights.com/forklift/)

Please refer to instructions from your sequencing service provider for this.  

**Note: Download all files to the `~/Workspace` directory.**

### Organizing the reads

Your raw sequencing reads may not necessarily always be in the proper staR-friendly format - one file per sample if single-end sequencing, two files per sample if paired-end sequencing. Fortunately, *staR-helper* can help you organize them properly.

Let's assume you have 9 paired-end sequencing libraries, each with 9 parts. Your raw reads files will look as follows:
```
╭─ubuntu@ip-172-31-36-39  ~/Workspace/raw_reads
╰─$ ls
SAMPLE001_L001_R1_001.fastq.gz  SAMPLE004_L001_R1_001.fastq.gz  SAMPLE007_L001_R1_001.fastq.gz
SAMPLE001_L001_R1_002.fastq.gz  SAMPLE004_L001_R1_002.fastq.gz  SAMPLE007_L001_R1_002.fastq.gz
SAMPLE001_L001_R1_003.fastq.gz  SAMPLE004_L001_R1_003.fastq.gz  SAMPLE007_L001_R1_003.fastq.gz
SAMPLE001_L001_R1_004.fastq.gz  SAMPLE004_L001_R1_004.fastq.gz  SAMPLE007_L001_R1_004.fastq.gz
SAMPLE001_L001_R1_005.fastq.gz  SAMPLE004_L001_R1_005.fastq.gz  SAMPLE007_L001_R1_005.fastq.gz
SAMPLE001_L001_R1_006.fastq.gz  SAMPLE004_L001_R1_006.fastq.gz  SAMPLE007_L001_R1_006.fastq.gz
SAMPLE001_L001_R1_007.fastq.gz  SAMPLE004_L001_R1_007.fastq.gz  SAMPLE007_L001_R1_007.fastq.gz
SAMPLE001_L001_R1_008.fastq.gz  SAMPLE004_L001_R1_008.fastq.gz  SAMPLE007_L001_R1_008.fastq.gz
```

Using the `compile` command within *staR-helper*:
```
╭─ubuntu@ip-172-31-36-39  ~/Workspace
╰─$ staR-helper compile -p '(?P<sample>.+?)_R(?P<read>\d)_(?P<part>\d{1,3})\.(?P<extension>fastq\.gz)' -i raw_reads -o reads
```
```
╭─ubuntu@ip-172-31-36-39  ~/Workspace
╰─$ tree -r reads
reads
├── SAMPLE009_L002
│   ├── SAMPLE009_L002_R2.fastq.gz
│   └── SAMPLE009_L002_R1.fastq.gz
├── SAMPLE009_L001
│   ├── SAMPLE009_L001_R2.fastq.gz
│   └── SAMPLE009_L001_R1.fastq.gz
├── SAMPLE008_L002
│   ├── SAMPLE008_L002_R2.fastq.gz
│   └── SAMPLE008_L002_R1.fastq.gz
├── SAMPLE008_L001
│   ├── SAMPLE008_L001_R2.fastq.gz
│   └── SAMPLE008_L001_R1.fastq.gz
├── SAMPLE007_L002
│   ├── SAMPLE007_L002_R2.fastq.gz
│   └── SAMPLE007_L002_R1.fastq.gz
├──...
```

As you can see all the file parts have been concatenated, and organized properly into the respective sample directories within the `reads` directory.  

Let's break down the arguments passed to `staR-helper compile`:

- `-p '(?P<sample>.+?)_R(?P<read>\d)_(?P<part>\d{1,3})\.(?P<extension>fastq\.gz)'`: this is the regular expression pattern used to parse the names of the raw reads files.
- `-i raw_reads`: the path to the directory containing the reads to be organized.
- `-o reads`: the path to the directory to write the organized reads to

You can and should consult the help menu for more information:
```
$ staR-helper compile --help
```

### Using the staR-pipeline

Once your files are properly organized, you are ready to start the alignment. Let's configure the settings for the `STARproject`. The R script to run the pipeline is at `~/staR_run.R`. Let's open it in a command-line editor to make the necessary changes:
```
$ nano ~/staR_run.R
```

The following configuration options are required:

- `genomeFasta`: the path to the fasta file containing the genome of interest. Genomes have been stored at `~/genomes`.
- `annotation`: the path to the GTF annotation file that accompanies the genome.
- `inputDir`: the path to your organized reads. This is set to `reads` by default.
- `inputPattern`: the regular expression pattern used to identify your fastq files, with parentheses around the READ number, if paired-end sequencing was performed.
- `pairedEnd`: whether or not paired-end sequencing was performed.
- `ncrna`: whether or not you are aligning a small RNA library.
- `readLength`: the length of the longest read in the library.
- `uncompressInput`: `zcat` if files are gzipped. Otherwise, leave this blank.

Once you are done editing this file, hit `Ctrl-x` to exit and choose to save the file.

Start the alignment process:
```
$ ~/staR_run.R
```

This will start the process as a daemon in the background. To view the progress, type this at the prompt:
```
$ screen -rx staR
```

### Results

The last step of the *staR-pipeline* is the enumeration of alignments to annotations/features in the specified GTF file, yielding a `SummarizedExperiment` R object containing an (n x m) matrix of raw counts  where n (rows) corresponds to the number of features, and m (columns) corresponds to the number of samples.  

This object is written out to `counts.rda` by default, if you left the `countsPath` option untouched in the `STARproject` settings. This may be used for downstream analyses such as:

- differential expression analysis (using the *DESeq2* or *limma* Bioconductor R packages)
- constructing heatmaps
- principal component analysis

Using [Cyberduck](https://cyberduck.io/), [Forklift](http://www.binarynights.com/forklift/) or the `rsync` command, copy the `counts.rda` file to your local system. You may also choose to copy the following:

- Alignments - `SAMPLE_Aligned.sortedByCoord.out.bam`
- Alignment statistics - `SAMPLE_Log.final.out`

These are located in the respective sample directories within the `output` directory.

### Closing the session

Upon retrieving the necessary files, it is good practice to shut down and terminate the instance to avoid unnecessary costs. Exit the shell of the instance by issuing the `exit` command, and you should now be back at your own prompt. Navigate to the `star-helper` directory and type the following:
```
$ ./staR-helper terminate
```
Once you see a list of your instance(s) that have been queued for termination, you are done.
